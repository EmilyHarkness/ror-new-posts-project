Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'posts#index'

  resources :posts do
    resources :comments
  end
  resources :authors, only: %i[new create show]
  resources :votes

  get 'signup', to: 'authors#new'
  get '/signin', to: 'sessions#new'
  post '/signin', to: 'sessions#create'
  get 'signout', to: 'sessions#destroy'
end
