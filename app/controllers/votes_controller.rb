class VotesController < ApplicationController
  before_action :set_vote, only: %i[vote_up vote_down update]

  def new
    @vote = Vote.new
  end

  def create
    @vote = Vote.create(vote_params)
    @vote.save
    if @vote.save
      redirect_to post_path(@vote.comment.post)
    else
      redirect_to 'new'
    end
  end

  def vote_up; end

  def vote_down; end

  def update
    if @vote.update(vote_params)
      redirect_to post_path(@vote.comment.post)
    end
  end

  private

  def set_vote
    @vote = Vote.find(params[:id])
  end

  def vote_params
    params.require(:vote).permit(:comment_id, :author_id, :value)
  end
end
