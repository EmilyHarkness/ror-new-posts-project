class CommentsController < ApplicationController
  before_action :set_variables, only: %i[edit update destroy]

  def new
    @comment = Comment.new
  end

  def create
    @comment = Comment.create(comment_params)
    @comment.save
    if @comment.save
      redirect_to post_path(@comment.post)
      flash[:success] = 'You have created comment'
    else
      redirect_to root_path
    end
  end

  def edit
    redirect_to post_path(@post) if !current_author.present? || @comment.author_id != current_author.id || Time.now - @comment.created_at > 3600
  end

  def update
    if @comment.update(comment_params)
      redirect_to post_path(@post)
      flash[:success] = 'You have updated comment'
    else
      render 'edit'
    end
  end

  def destroy
    @comment.destroy
    redirect_to post_path(@post)
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :post_id, :author_id)
  end

  def set_variables
    @comment = Comment.find(params[:id])
    @post = Post.find(@comment.post_id)
  end
end
