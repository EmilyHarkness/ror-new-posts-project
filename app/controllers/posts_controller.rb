class PostsController < ApplicationController
  include ApplicationHelper
  before_action :set_post, only: %i[show edit update destroy]

  impressionist actions: [:show], unique: [:session_hash]

  def index
    @posts = params[:search] ? Post.search(params[:search]) : Post.all.sort_by(&:updated_at)
  end

  def show
    @comment = Comment.new
    @comments = @post.comments
  end

  def new
    if current_author.present?
      @post = Post.new
    else
      redirect_to signin_path
    end
  end

  def create
    @post = Post.create(post_params)
    @post.save!
    if @post.save
      redirect_to @post
    else
      render 'new'
    end
  end

  def edit
    redirect_to post_path(@post) if !current_author.present? || @post.author_id != current_author.id
  end

  def update
    if @post.update(post_params)
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    redirect_to posts_path
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :content, :author_id,
                                 :image, :views_count)
  end
end
