class SessionsController < ApplicationController

  def new
    redirect_to root_path if current_author.present?
  end

  def create
    author = Author.find_by_email(params[:session][:email])
    if author && author.authenticate(params[:session][:password])
      session[:author_id] = author.id
      redirect_to root_path, notice: "Logged in!"
    else
      flash.now[:alert] = "Email or password is invalid"
      render "new"
    end
  end

  def destroy
    session[:author_id] = nil
    redirect_to root_path, notice: "Logged out!"
  end
end
