module ApplicationHelper
  def author_full_name(first_name, last_name)
    "#{first_name} #{last_name}"
  end
end
