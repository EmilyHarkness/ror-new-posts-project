class Post < ApplicationRecord
  belongs_to :author
  has_many :comments, dependent: :destroy

  has_one_attached :image

  is_impressionable

  def self.search(search)
    where(("title LIKE ? OR content LIKE ?"), "%#{search}%", "%#{search}%")
  end
end
